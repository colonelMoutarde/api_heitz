<?php

/*
 * Copyright (C) 2016 YesWeDev.
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301  USA
 *
 *  @author YesWeDev <contacrarobaseyeswedev.fr>
 * @copyright (c) 2016
 * @license GNU/LGPL http://www.gnu.org/licenses/lgpl.html 
 */

namespace ApiHeitz\Core;

interface CoreInterface
{


    /**
     * @return mixed
     */
    public function getCredential();
    /**
     * 
     * @param int $idSession
     * @param int $userId
     * @param string $type
     */
    public function setConnectHeitzAPI($idSession, $userId, $type);

    /**
     * 
     * @param string $newFields
     * @param string $pathToCookie
     */
    public function callWSHeitz($newFields, $pathToCookie);
}
